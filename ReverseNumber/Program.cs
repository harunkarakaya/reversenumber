﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReverseNumber
{
    class Program
    {
        static void Main(string[] args)
        {       
            Console.Write("Sayının ters çevirilmiş hali:" + reverseNumb(inputNumber()).ToString());
            Console.ReadLine();
        }

        static char[] inputNumber()
        {
            Console.Write("Ters çevirmek istediğiniz sayıyı giriniz:");
            char[] inputNumbers = Console.ReadLine().ToArray(); //int

            return inputNumbers;
        }

        static int reverseNumb(char[] param_inputNumb)
        {
            int charCount = 0;

            char[] reverseArray = new char[param_inputNumb.Length];

            for (int i = param_inputNumb.Length; i > 0; i--)
            {
                reverseArray[charCount] = param_inputNumb[i - 1];
                charCount++;
            }

            string reverseString = "";

            for (int i = 0; i < param_inputNumb.Length; i++)
            {
                reverseString += reverseArray[i];
            }

            int reverseInt = Convert.ToInt32(reverseString);

            return reverseInt;
        }
    }
}
